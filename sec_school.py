
schools=["NANYANG GIRLS' HIGH SCHOOL",
"RAFFLES GIRLS' SCHOOL (SECONDARY)",
"PAYA LEBAR METHODIST GIRLS' SCHOOL (SECONDARY)",
"Hwa Chong Institution SCHOOL",
"Raffles Institution",
"CHIJ St. Nicholas Girls' School",
"Anglo-Chinese School (Independent)",
"National Junior College",
"Dunman High School SCHOOL",
"CEDAR GIRLS' SECONDARY SCHOOL",
"Catholic High School SCHOOL",
"SAINT JOSEPH'S INSTITUTION",
"Singapore Chinese Girls’ School",
"Victoria School",
"River Valley High School SCHOOL",
"PAYA LEBAR METHODIST GIRLS' SCHOOL (SECONDARY)",
"Temasek Junior College",
"Anderson Secondary School",
"BUKIT PANJANG GOVERNMENT HIGH SCHOOL",
"Nan Chiau High School",
"Nan Hua High School",
"CHIJ Secondary (Toa Payoh)",
"Chung Cheng High School",
"Fairfield Methodist School (Secondary)",
"CRESCENT GIRLS' SCHOOL",
"St Margaret's Secondary School",
"Anglo-Chinese School (Barker Road)",
"Swiss Cottage Secondary School",
"Anglican High School",
"Chung Cheng High School (Yishun)",
"Ngee Ann Secondary School",
"Commonwealth Secondary School",
"Maris Stella High School",
"Yishun Town Secondary School",
"CHIJ Saint Theresa's Convent",
"Zhonghua Secondary School",
"Saint Andrew's Secondary School",
"Xinmin Secondary School",
"Presbyterian High School",
"Fuhua Secondary School",
"CHIJ Saint Joseph's Convent",
"Kuo Chuan Presbyterian Secondary School",
"Temasek Secondary School",
"Tanjong Katong Girls' School",
"Tanjong Katong Secondary School",
"Clementi Town Secondary School",
"Riverside Secondary School",
"Dunman Secondary School",
"Kranji Secondary School",
"Holy Innocents' High School",
"Edgefield Secondary School",
"CHIJ Katong Convent",
"Ang Mo Kio Secondary School",
"Evergreen Secondary School",
"Bowen Secondary School",
"Geylang Methodist School (Secondary)",
"Jurong Secondary School",
"Bukit Batok Secondary School",
"Gan Eng Seng School",
"Saint Anthony's Canossian Secondary School",
"St. Gabriel's Secondary School",
"Hua Yi Secondary School",
"Saint Hilda's Secondary School",
"West Spring Secondary School",
"Hai Sing Catholic School",
"Mayflower Secondary School",
"Pei Hwa Secondary School",
"Ahmad Ibrahim Secondary School",
"Pasir Ris Secondary School",
"Pasir Ris Crest Secondary School",
"Deyi Secondary School",
"Queensway Secondary School",
"Unity Secondary School",
"Bedok View Secondary School",
"Woodlands Ring Secondary School",
"Beatty Secondary School",
"Chua Chu Kang Secondary School",
"Compassvale Secondary School",
"Peirce Secondary School",
"Meridian Secondary School",
"North Vista Secondary School",
"Orchid Park Secondary School",
"Yuan Ching Secondary School",
"Bedok South Secondary School",
"Kent Ridge Secondary School",
"Zhenghua Secondary School",
"Bukit View Secondary School",
"Hillgrove Secondary School",
"Woodgrove Secondary School",
"Montfort Secondary School",
"Greendale Secondary School",
"Tampines Secondary School",
"Christ Church Secondary School",
"Yishun Secondary School",
"Jurong West Secondary School",
"Seng Kang Secondary School",
"Westwood Secondary School",
"Admiralty Secondary School",
"Juying Secondary School",
"Naval Base Secondary School",
"Jurongville Secondary School",
"Punggol Secondary School",
"Hougang Secondary School",
"Regent Secondary School",
"Springfield Secondary School",
"Ping Yi Secondary School",
"Bukit Merah Secondary School",
"Dunearn Secondary School",
"Changkat Changi Secondary School",
"New Town Secondary School",
"Loyang View Secondary School",
"Bartley Secondary School",
"Serangoon Secondary School",
"Tanglin Secondary School",
"Bendemeer Secondary School",
"Peicai Secondary School",
"Queenstown Secondary School",
"Damai Secondary School",
"Yusof Ishak Secondary School",
"Yuhua Secondary School",
"Serangoon Garden Secondary School",
"Fuchun Secondary School",
"Whitley Secondary School",
"Woodlands Secondary School",
"Bedok Green Secondary School",
"Broadrick Secondary School",
"Fajar Secondary School",
"Teck Whye Secondary School",
"Junyuan Secondary School",
"Outram Secondary School",
"Northland Secondary School",
"Boon Lay Secondary School",
"Yuying Secondary School",
"Guangyang Secondary School",
"Northbrooks Secondary School",
"Yio Chu Kang Secondary School",
"Greenridge Secondary School",
"Manjusri Secondary School",
"Assumption English School",
"East Spring Secondary School",
"Marsiling Secondary School",
"Canberra Secondary School",
"Sembawang Secondary School",
"East View Secondary School",
"Hong Kah Secondary School ",
"Shuqun Secondary School"
]

payload =['https://developers.onemap.sg/commonapi/search?searchVal={}&returnGeom=Y&getAddrDetails=Y&pageNum=1'.format(s.strip().lower()) for s in schools]
import asyncio
import requests
import json

async def main():
    loop = asyncio.get_event_loop()
    fut=[]
    res=[]
    out=[]
    for p in payload:
        fut.append(loop.run_in_executor(None, requests.get, p))
    for f in fut:
        res.append(await f)
    counter=1
    for r in res:
        js = json.loads(r.text)
        try:
            for i in range(len(js['results'])):
                school = js['results'][i]['SEARCHVAL']
                lat = js['results'][i]['LATITUDE']
                lon =js['results'][i]['LONGTITUDE']
                out.append(str(counter)+'|'+school+'|'+str(i)+'|'+lat+'|'+lon)
                print(str(counter)+'|'+school+'|'+str(i)+'|'+lat+'|'+lon)
        except Exception as e:
            print(e)
            pass
        counter+=1
    return out
loop = asyncio.get_event_loop()
add_book = loop.run_until_complete(main())
with open('schoolinfo.txt', 'w') as f:
    for item in add_book:
        f.write("%s\n" % item)
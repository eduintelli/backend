from pandas import read_sql
from os import environ
from sqlalchemy import create_engine

def db_conn_str():
    if environ.get("DB_USER") is not None:
        db_user = environ.get("DB_USER")
        db_pass = environ.get("DB_PASS")
        db_name = environ.get("DB_NAME")
        cloud_sql_instance_name = environ.get("CLOUD_SQL_INSTANCE_NAME")
        # [START cloud_sql_mysql_connection_pool]
        # The SQLAlchemy engine will help manage interactions, including automatically
        # managing a pool of connections to your database
        SQLALCHEMY_DATABASE_URI = (
            'mysql+pymysql://{user}:{password}@localhost/{database}'
            #'mysql+pymysql://{user}:{password}@/{database}'
            '?unix_socket=/cloudsql/{connection_name}').format(
            user=db_user, password=db_pass,
            database=db_name, connection_name=cloud_sql_instance_name)
    else:
        SQLALCHEMY_DATABASE_URI = (
            'mysql+pymysql://{user}:{password}@127.0.0.1:3306/{database}').format(
            user='root', password='8801890',
            database='PSLE')
    return SQLALCHEMY_DATABASE_URI

db = create_engine(
    db_conn_str(),
    pool_size=50,
    max_overflow=20,
    pool_timeout=30,  # 30 seconds
    pool_recycle=1800,  # 30 minutes
    )

def insert_DB(tbl_name, cols, values):
    try:
        with db.connect() as conn:
            # Create a new record
            sql = "INSERT INTO "+ tbl_name +" (" +cols+ ") VALUES (" +values+ ")"
            conn.execute(sql)
    except Exception as e:
        return e
    return 'OK'

def read_DB(sql):
    try:
        data = read_sql(sql, db)
    except Exception as e:
        return e, None
    return 'OK', data

def dbtest():
    with db.connect() as conn:
        c = conn.execute(
            "SHOW TABLES"
        ).fetchall()
    length = len(c)
    if length > 0:
        return True
    else:
        return False



    

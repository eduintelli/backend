###########Import###############
import DB_Operation
import random
import numpy as np
from scipy.stats import skewnorm
from scipy import stats
from math import sin, cos, sqrt, atan2, radians
import pandas as pd

random.seed(43)

#####################Class of UserScore########################################
class uscore(object):
    def __init__(self, math_score,science_score,eng_score,mt_score,grade,term,school,sessionID,clientID):
        self.m_score=math_score
        self.s_score = science_score
        self.e_score = eng_score
        self.mt_score = mt_score
        self.grade = grade
        self.term = term
        self.school = school
        self.sessionID = sessionID
        self.clientID = clientID
        self.status = -1
        self.totalScore = float(self.m_score)+float(self.s_score)+float(self.e_score)+float(self.mt_score)
        self.InsertDB()

    def InsertDB(self):
        msg=DB_Operation.insert_DB('UserScoreCollection',"M_Score,S_Score,E_Score,MT_Score,Score_Grade,Term,PSchoolName,SessionID, ClientID",
                                     "\'{} \', \'{}\', \'{}\', \'{}\', \'{}\', \'{}\', \'{}\', \'{}\', \'{}\'".format(self.m_score,self.s_score,self.e_score,self.mt_score,self.grade,self.term,self.school,self.sessionID,self.clientID))
        if msg == 'OK':
            self.status = 'OK'
        else:
            self.status = msg

###Define Model####
class T_Score_model():
    def __init__(self):
        self.model=self.model_generate()

    def model_generate(self):
        a = -3
        l = 249.2
        s = 58
        r = skewnorm.rvs(a, l, s, size=1000000)

        r_less = r[r < 275]
        n = len(r) - len(r_less)
        r_more = skewnorm.rvs(0, loc=190, scale=10, size=int(n * 6 / 10))
        r_more2 = skewnorm.rvs(8, loc=275, scale=3, size=int(n * 4 / 10))
        r_recal2 = np.append(r_less, r_more)
        return np.append(r_recal2, r_more2)

##############################This Function Retrive the user info and score associated with a sessionID###########3
def T_Score_Cal(scoreCollection,school,r):
    #model parameter
    mean_e = 72 # english
    sd_e = 11
    mean_m = 67  # math
    sd_m = 16.8
    mean_mt = 81  # mt
    sd_mt = 12.5
    mean_s = 69.5  # science
    sd_s = 14.1

    #get school factor
    sql = "select Norm_Factor from PriSchool where School_Name = '" + school + "'"
    msg, data = DB_Operation.read_DB(sql)

    normFactor = float(data.iat[0, 0])

    #find max grade
    maxGrade = max(float(s.grade) for s in scoreCollection)

    #find total grade and avg grade per subject
    totalGrade =0
    avgM=0
    avgS=0
    avgMT=0
    avgE=0
    for s in scoreCollection: totalGrade+= float(s.grade)**3

    for s in scoreCollection:
        avgM += float(s.m_score) * (float(s.grade)**3/totalGrade)
        avgS += float(s.s_score) * (float(s.grade)**3 / totalGrade)
        avgMT += float(s.mt_score) * (float(s.grade)**3 / totalGrade)
        avgE += float(s.e_score) * (float(s.grade)**3 / totalGrade)


    #avgM=100
    #avgE=100
    #avgMT=100
    #avgS=100
    #normFactor=0.825

    #estimate T_Score:
    def normGrad(f, x):
        if f >= 1:
            delta = (f - 1) / 2
            lower, upper = 0, 20
            x_nom = ((100 - x) / 100)
            x_nom = (lower + (upper - lower) * x_nom)
            return ((1 / (1 + np.exp(-x_nom))) ** 2) * delta + 1
        else:
            delta = (1 - f) / 2.5
            lower, upper = 0, 20
            x_nom = ((100 - x) / 100)
            x_nom = (lower + (upper - lower) * x_nom)
            return 1 - ((1 / (1 + np.exp(-x_nom))) ** 2) * delta

    mTscore = (50 + 10 * (avgM - mean_m) / sd_m)*normGrad(normFactor,avgM)
    eTscore = (50 + 10 * (avgE - mean_e) / sd_e)*normGrad(normFactor,avgE)
    sTscore = (50 + 10 * (avgS - mean_s) / sd_s)*normGrad(normFactor,avgS)
    mtTscore = (50 + 10 * (avgMT - mean_mt) / sd_mt)*normGrad(normFactor,avgMT)
    Tscore = (mTscore+eTscore+sTscore+mtTscore)
    if Tscore > 293: Tscore=293

    Percent =stats.percentileofscore(r.model, Tscore)

    #estimate T_Score stdev:
    if maxGrade == 6:
        default_std = 9.65
    elif maxGrade == 5:
        default_std = 14.23
    elif maxGrade == 4:
        default_std = 20.43
    else:
        default_std = 30.44

    totalScoreList = [s.totalScore for s in scoreCollection]
    userStd = np.std(np.array(totalScoreList))

    if len(totalScoreList) > 1:
        nomStdev = (default_std + userStd) / 2
    else:
        nomStdev = default_std

    print(Tscore,nomStdev,Percent)
    return Tscore,nomStdev,Percent

class sec_post(object):
    def __init__(self):
        #cdr
        self.model_1=[[1,2,20],[1,3,15],[3,4,10],[5,6,3],[7,8,2],[10,10,2]]

def sec_chance_cal(cutoff, score, stdev):
   return np.clip(1-stats.norm(score, stdev).cdf(cutoff),0.00,0.95)

def sec_post_chance_cal(cutoff, score):
   return np.clip(stats.norm(cutoff, 2.5).cdf(score),0.00,1)

def sec_post_cal(c,d,r,sec_pd):
    sec_pd['result']=sec_pd[['chance','dist','cut_off']].apply(lambda x: float(x[0])*c+(float(x[1])*d+float(x[2])*r), axis=1)
    sec_pd=sec_pd[sec_pd.chance>0.05]
    sec_pd.sort_values(by=['result'], ascending=False, inplace=True)
    #'School_Name', 'lat', 'lon', 'cut_off', 'gender', 'chance', 'dist', 'result'
    pd.set_option('max_colwidth', 800)
    pd.set_option('display.max_rows', 500)
    pd.set_option('display.max_columns', 500)
    pd.set_option('display.width', 1000)
    return sec_pd.iat[0,0],sec_pd.iat[0,1],sec_pd.iat[0,2],sec_pd.iat[0,3],sec_pd.iat[0,4],sec_pd.iat[0,5],sec_pd.iat[0,6],sec_pd.iat[0,7]

def sec_dist_cal(lat1, lon1, lat2,lon2):
    R = 6373.0
    lat1 = radians(lat1)
    lon1 = radians(lon1)
    lat2 = radians(lat2)
    lon2 = radians(lon2)

    dlon = lon2 - lon1
    dlat = lat2 - lat1

    a = sin(dlat / 2) ** 2 + cos(lat1) * cos(lat2) * sin(dlon / 2) ** 2
    c = 2 * atan2(sqrt(a), sqrt(1 - a))

    distance = R * c
    return distance

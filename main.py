from DB_Operation import dbtest, read_DB
import Class
from flask_cors import CORS
from flask import Flask,request,jsonify,make_response,render_template


app = Flask(__name__)
CORS(app)
model = Class.T_Score_model()
sql = "select School_Name, Lat as lat, lon as lon, cut_off, gender from SecSchool"
__, sec_school = read_DB(sql)
sql = "select School_Name from PriSchool"
__, pri_school = read_DB(sql)

@app.route('/')
def hello():
    """Return a friendly HTTP greeting."""
    return "basic server load success"

key = 'gYdK$q12@as'

@app.route('/status', methods=['GET'])
def get_status():

    status=dbtest()
    if status:
        STATUS = [{'DB_STATUS': 'DB Connection Successful'}]
    else:
        STATUS = [{'DB_STATUS': 'DB Connection Failed'}]
    return jsonify({'STATUS': STATUS}), 200


@app.route('/Get_PSch', methods=['POST'])
def Get_PSch():
    data = request.get_json(force=True)
    if data['key']==key:
        try:
            school_name = list(pri_school.iloc[:, 0])
            return jsonify({'status': 'OK','schools':school_name }), 200
        except Exception as e:
            print(e)
            return jsonify({'status': e}), 400
    else:
        return jsonify({'status': 'Wrong Key'}), 401

@app.route('/Get_SSch', methods=['POST'])
def Get_SSch():
    data = request.get_json(force=True)
    if data['key']==key:
        try:
            school_name = list(sec_school.iloc[:, 0])
            return jsonify({'status': 'OK','schools':school_name }), 200
        except Exception as e:
            print(e)
            return jsonify({'status': e}), 400
    else:
        return jsonify({'status': 'Wrong Key'}), 401

@app.route('/create_scores', methods=['POST'])
def create_scores():
    data = request.get_json(force=True)
    scoreCollection=[]
    if data['key'] == key:
        #Input database
        try:
            for s in data['scores']:
                myscore = Class.uscore(s['math'], s['science'],
                                       s['eng'], s['mt'], s['grade'],
                                       s['term'],
                                       data['school'],
                                       data['sessionID'],
                                       data['clientID'])
                scoreCollection.append(myscore)
        except Exception as e:
            errStm='Insert Score to DB Error:' + str(e)
            return jsonify({'status': errStm}), 400
        # Input Calculation
        try:
            score, std,percent = Class.T_Score_Cal(scoreCollection,data['school'],model)
        except Exception as e:
            errStm='Calculation Error:' + str(e)
            return jsonify({'status': errStm}), 400
        return jsonify({'status': 'OK', 'score':score, 'percent':percent, 'stdev':std}), 200
    else:
        return jsonify({'status': 'Wrong Key'}), 401

@app.route('/create_scores_w_counter', methods=['POST'])
def create_scores_w_counter():
    data = request.get_json(force=True)
    scoreCollection=[]
    if data['key'] == key:
        #Input database
        try:
            for s in data['scores']:
                myscore = Class.uscore(s['math'], s['science'],
                                       s['eng'], s['mt'], s['grade'],
                                       s['term'],
                                       data['school'],
                                       data['sessionID'],
                                       data['clientID'])
                scoreCollection.append(myscore)
        except Exception as e:
            errStm='Insert Score to DB Error:' + str(e)
            return jsonify({'status': errStm}), 400
        # Input Calculation

        sql = "select count(*) from UserScoreCollection where ClientID = '" + data['clientID'] + "'"
        msg, count_df = read_DB(sql)
        count = float(count_df.iat[0, 0])
        try:
            if count <=3:
                score, std,percent = Class.T_Score_Cal(scoreCollection,data['school'],model)
                return jsonify({'status': 'OK', 'score': score, 'percent': percent, 'stdev': std}), 200
            else:
                return jsonify({'status': 'Exceed Count'}), 200
        except Exception as e:
            errStm='Calculation Error:' + str(e)
            return jsonify({'status': errStm}), 400
    else:
        return jsonify({'status': 'Wrong Key'}), 401

@app.route('/cal_sec', methods=['POST'])
def cal_sec():
    data = request.get_json(force=True)
    score,stdev,lat,lon,key = data['score'],data['stdev'],data['lat'],data['lng'],data['key']
    if data['key'] == key:
        sec_sch_pd=sec_school.iloc[:,0:4]
        sec_sch_pd['chance']=sec_sch_pd[['cut_off']].apply(lambda x: Class.sec_chance_cal(float(x),float(score),float(stdev)), axis=1)
        sec_sch_pd['dist'] = sec_sch_pd[['lat','lon']].apply(lambda x: Class.sec_dist_cal(float(x[0]),float(x[1]), float(lat), float(lon)), axis=1)
        sec_sch_pd.sort_values(by=['dist'],ascending=True,inplace =True)
        list=[]
        for n in range (3):
            list.append({'id':n,'sec_sch_name':sec_sch_pd.iat[n,0],'lat':sec_sch_pd.iat[n,1],'lng':sec_sch_pd.iat[n,2],'chance':sec_sch_pd.iat[n,4]*100})
        return jsonify(list),200
    else:
        return jsonify({'status': 'Wrong Key'}), 401

@app.route('/cal_sec_list', methods=['POST'])
def cal_sec_list():
    data = request.get_json(force=True)
    score,stdev,sec_sch_list,key = data['score'],data['stdev'],data['sec_sch_list'],data['key']
    if data['key'] == key:
        sec_sch_pd = sec_school.iloc[:,0:4]
        print(sec_sch_pd.columns)
        sec_sch_pd['chance']=sec_sch_pd[['cut_off']].apply(lambda x: Class.sec_chance_cal(float(x),float(score),float(stdev)), axis=1)
        sec_sch_pd=sec_sch_pd[sec_sch_pd.School_Name.isin(sec_sch_list)]
        list=[]
        for n in range(len(sec_sch_pd)):
            list.append({'id':n,'sec_sch_name':sec_sch_pd.iat[n,0],'lat':sec_sch_pd.iat[n,1],'lng':sec_sch_pd.iat[n,2],'chance':sec_sch_pd.iat[n,4]*100})
        return jsonify(list),200
    else:
        return jsonify({'status': 'Wrong Key'}), 401

@app.route('/cal_sec_post', methods=['POST'])
def cal_sec_post():
    data = request.get_json(force=True)
    score,lat,lon,key,gender = float(data['score'])+3,data['lat'],data['lng'],data['key'],data['gender']
    if data['key'] == key:
        sec_sch_pd=sec_school[(sec_school['gender']==str(data['gender'])) | (sec_school['gender']=='co-ed')]
        sec_sch_pd['chance']=sec_sch_pd[['cut_off']].apply(lambda x: Class.sec_post_chance_cal(float(x),float(score)), axis=1)
        sec_sch_pd['dist'] = sec_sch_pd[['lat','lon']].apply(lambda x: Class.sec_dist_cal(float(x[0]),float(x[1]), float(lat), float(lon)), axis=1)
        sec_sch_pd.sort_values(by=['cut_off'],ascending=False,inplace =True)
        sec_sch_pd['cut_off'] = (sec_sch_pd['cut_off'] - sec_sch_pd['cut_off'].min()) / (sec_sch_pd['cut_off'].max() - sec_sch_pd['cut_off'].min())
        sec_sch_pd['dist'] = (sec_sch_pd['dist'] - sec_sch_pd['dist'].min()) / (
                    sec_sch_pd['dist'].max() - sec_sch_pd['dist'].min())
        sec_sch_pd['dist'] = 1-sec_sch_pd['dist']
        list=[]
        for n in range (6):
            post_model = Class.sec_post()
            c,d,r=post_model.model_1[n][0],post_model.model_1[n][1],post_model.model_1[n][2]
            exist_sec_list=[s['sec_sch_name'] for s in list]
            query_sec_list = sec_sch_pd[~sec_sch_pd.School_Name.isin(exist_sec_list)]
            School_Name, lat, lon, cut_off, gender, chance, dist,result = Class.sec_post_cal(c,d,r,query_sec_list)
            list.append({'id':n,'sec_sch_name':School_Name,'lat':lat,'lng':lon,'cut_off':cut_off,'gender':gender,'chance':chance,'dist':dist,'cum':result})
        return jsonify(list),200
    else:
        return jsonify({'status': 'Wrong Key'}), 401

@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify({'STATUS': 'Not found'}), 404)

if __name__ == '__main__':
    app.run(host='127.0.0.1', port=8080, debug=True)



